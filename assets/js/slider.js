jQuery(document).ready(function($) {
    $(".owl-carousel").each(function() {
        $(this).owlCarousel({
            items: $(this).data('items') ? $(this).data('items') : '1',
            slideBy: $(this).data('slideby') ? $(this).data('slideby') : '1',
            autoplay: $(this).data('autoplay') ? true : false,
            autoplaySpeed: $(this).data('autoplayspeed'),
            nav: $(this).data('nav') ? true : false,
            dots: $(this).data('dots') ? true : false,
            loop: $(this).data('loop') ? true : false,
            center: $(this).data('center') ? true : false,
            mouseDrag: $(this).data('mouseDrag') ? true : false,
            touchDrag: $(this).data('touchDrag') ? true : false,
        });
    });
});
