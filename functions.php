<?php
define('TEXTDOMAIN', 'theme');

require_once( 'vendor/wp-less/wp-less.php' );
require_once( 'inc/post_types.php' );
require_once( 'inc/shortcodes.php' );

function wp_enqueue_scripts_handler()
{

    wp_enqueue_style('style', get_stylesheet_directory_uri().'/style.css');
    wp_enqueue_style('main', get_stylesheet_directory_uri().'/less/main.less');
    wp_enqueue_script('scripts', get_stylesheet_directory_uri().'/assets/js/slider.js', array('jquery'), null, true);
    wp_enqueue_script('scripts', get_stylesheet_directory_uri().'/assets/js/scripts.js', array('jquery'), null, true);

    // jquery.matchHeight.js
    wp_enqueue_script('matchheight', get_stylesheet_directory_uri().'/assets/js/jquery.matchHeight.js', array('jquery'), null, true);

    // Owl carousel js (v2)
    wp_enqueue_style('owl', get_stylesheet_directory_uri().'/assets/js/owl.carousel.js/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-theme-default', get_stylesheet_directory_uri().'/assets/js/owl.carousel.js/assets/owl.theme.default.min.css');
    wp_enqueue_script('owl', get_stylesheet_directory_uri().'/assets/js/owl.carousel.js/owl.carousel.min.js', array('jquery'), null, true);

}
add_action( 'wp_enqueue_scripts', 'wp_enqueue_scripts_handler' );


function after_theme_setup_handler()
{
    register_nav_menus(array(
		'primary' => __('Primary'),
		'footer' => __('Footer'),
	));

    add_theme_support( 'custom-logo' );

    load_theme_textdomain( TEXTDOMAIN, get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'after_theme_setup_handler' );

function ok_custom_logo() {

	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}
