        </div>
        <footer class="footer">
            <div class="container clearfix footer_top_container layout_3_col">
                <div class="footer_column">
                    <?php dynamic_sidebar('footer_col_1') ?>
                </div>
                <div class="footer_column">
                    <?php dynamic_sidebar('footer_col_2') ?>
                </div>
                <div class="footer_column">
                    <?php dynamic_sidebar('footer_col_3') ?>
                </div>
            </div>
            <div class="footer_bottom fw">
                <div class="container clearfix footer_bottom_container">
                    <span><?php echo __('(C) 2017. All rights reserved', TEXTDOMAIN) ?></span>
                </div>
            </div>
        </footer>
        </div>
    <?php wp_footer(); ?>
    </body>
</html>
