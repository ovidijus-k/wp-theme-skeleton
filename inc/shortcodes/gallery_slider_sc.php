<?php
function gallery_slider_sc($atts)
{
    extract($atts);
    if (!$images)
        return '';
    else
        $images = explode(',', $images);

    ob_start(); ?>
    <div class="gallery_slider owl-carousel" <?php echo gs_get_data_atts($atts) ?>>
        <?php foreach($images as $image_id) : ?>
            <div class="item">
                <?php echo wp_get_attachment_image($image_id, 'full') ?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php $output = ob_get_clean();
    return $output;
}

function gs_get_data_atts($atts)
{
    $excluded_atts = array(
        'images',
    );

    $data_atts_html = "";
    foreach ($atts as $att_key => $att_val) {
        $data_atts_html .= "data-" .$att_key . '="' . $att_val . '" ';
    }
    $data_atts_html = rtrim($data_atts_html);
    return $data_atts_html;
}
