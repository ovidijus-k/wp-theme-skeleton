<?php
function button_sc($atts)
{
    extract($atts);
    $vc_link = vc_build_link($url);
    ob_start(); ?>
    <a class="btn"
        href="<?php echo isset($vc_link['url']) ? $vc_link['url'] : '' ?>"
        target="<?php echo isset($vc_link['target']) ? $vc_link['target'] : '' ?>"
        rel="<?php echo isset($vc_link['rel']) ? $vc_link['rel'] : '' ?>">
        <?php echo isset($vc_link['title']) ? $vc_link['title'] : '' ?>
    </a>
    <?php $output = ob_get_clean();
    return $output;
}
