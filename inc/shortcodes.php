<?php
function register_sc() {
    $shortcodes = array(
        array(
            "name" => "Button",
            "base" => "button_sc",
            "params" => array(
                array(
                    "type" => "vc_link",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "URL", TEXTDOMAIN ),
                    "param_name" => "url",
                ),
            ),
        ),
        array(
            "name" => "Gallery Slider",
            "base" => "gallery_slider_sc",
            "params" => array(
                array(
                    "type" => "attach_images",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Images", TEXTDOMAIN ),
                    "param_name" => "images",
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Items", TEXTDOMAIN ),
                    "param_name" => "items",
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Slide by", TEXTDOMAIN ),
                    "param_name" => "slideBy",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Autoplay", TEXTDOMAIN ),
                    "param_name" => "autoplay",
                ),
                array(
                    "type" => "textfield",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Autoplay speed", TEXTDOMAIN ),
                    "param_name" => "autoplaySpeed",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Nav", TEXTDOMAIN ),
                    "param_name" => "nav",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Dots", TEXTDOMAIN ),
                    "param_name" => "dots",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Loop", TEXTDOMAIN ),
                    "param_name" => "loop",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Center", TEXTDOMAIN ),
                    "param_name" => "center",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Mouse drag", TEXTDOMAIN ),
                    "param_name" => "mouseDrag",
                ),
                array(
                    "type" => "checkbox",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __( "Touch drag", TEXTDOMAIN ),
                    "param_name" => "touchDrag",
                ),
            ),
        ),
    );

    foreach($shortcodes as $sc) {
        require_once('shortcodes/' . $sc['base'] . '.php');
        if (function_exists('vc_map')) {
            vc_map($sc);
        }
        add_shortcode($sc['base'], $sc['base']);
    }

}

if (has_action('vc_before_init')) {
    add_action('vc_before_init', 'register_sc');
} else {
    add_action('init', 'register_sc');
}
